<?php

use Chetkov\CurrencyRates\Application\ExchangeRates\CbrExchangeRatesProvider;
use Chetkov\CurrencyRates\Application\ExchangeRates\ExchangeRatesProviderCachingDecorator;
use Chetkov\CurrencyRates\Application\ExchangeRates\ExchangeRatesProviderPersistingDecorator;
use Chetkov\CurrencyRates\Application\ExchangeRates\PrivateBankExchangeRatesProvider;
use Chetkov\CurrencyRates\Application\ExchangeRates\Translator\ExchangeRateDTOTranslator;
use Chetkov\CurrencyRates\Infrastructure\Cache\ArrayCache;
use Chetkov\CurrencyRates\Infrastructure\Db\ExchangeRatesArrayRepository;
use Chetkov\CurrencyRates\Infrastructure\Logger\ConsoleLogger;
use \Chetkov\CurrencyRates\Infrastructure\ExchangeRates;

require_once __DIR__ . '/vendor/autoload.php';

// Инстанцируем инфраструктуру
$logger = new ConsoleLogger();
$cache = new ArrayCache();
$repository = new ExchangeRatesArrayRepository();
$rateTranslator = new ExchangeRateDTOTranslator();


// Инстанцируем реального поставщика курсов валют
$cbrClient = new ExchangeRates\Cbr\ApiClient($logger);
$cbrRatesProvider = new CbrExchangeRatesProvider($cbrClient, $rateTranslator);

// ИЛИ
$privateBankClient = new ExchangeRates\PrivateBank\ApiClient($logger);
$privateBankRatesProvider = new PrivateBankExchangeRatesProvider($privateBankClient, $rateTranslator);

// ИЛИ еще какую-то другую реализацию

// Декорируем реального провайдера, для сохранения полученных курсов в постоянное хранилище
$cbrRatesPersistingProvider = new ExchangeRatesProviderPersistingDecorator($cbrRatesProvider, $repository);
// Декорируем PersistingDecorator, для сохранения курсов в кэш
$cbrRatesCachingProvider = new ExchangeRatesProviderCachingDecorator($cbrRatesPersistingProvider, $cache, 'v1', 10);

// Пользуемся
$rates = $cbrRatesCachingProvider->getRates();

