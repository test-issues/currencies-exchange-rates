## Использование

```php
<?php

use Chetkov\CurrencyRates\Application\ExchangeRates\CbrExchangeRatesProvider;
use Chetkov\CurrencyRates\Application\ExchangeRates\ExchangeRatesProviderCachingDecorator;
use Chetkov\CurrencyRates\Application\ExchangeRates\ExchangeRatesProviderPersistingDecorator;
use Chetkov\CurrencyRates\Application\ExchangeRates\PrivateBankExchangeRatesProvider;
use Chetkov\CurrencyRates\Application\ExchangeRates\Translator\ExchangeRateDTOTranslator;
use Chetkov\CurrencyRates\Infrastructure\Cache\ArrayCache;
use Chetkov\CurrencyRates\Infrastructure\Db\ExchangeRatesArrayRepository;
use Chetkov\CurrencyRates\Infrastructure\Logger\ConsoleLogger;
use \Chetkov\CurrencyRates\Infrastructure\ExchangeRates;

require_once __DIR__ . '/vendor/autoload.php';

// Инстанцируем инфраструктуру
$logger = new ConsoleLogger();
$cache = new ArrayCache();
$repository = new ExchangeRatesArrayRepository();
$rateTranslator = new ExchangeRateDTOTranslator();

// Инстанцируем реального поставщика курсов валют
$cbrClient = new ExchangeRates\Cbr\ApiClient($logger);
$cbrRatesProvider = new CbrExchangeRatesProvider($cbrClient, $rateTranslator);

// ИЛИ
$privateBankClient = new ExchangeRates\PrivateBank\ApiClient($logger);
$privateBankRatesProvider = new PrivateBankExchangeRatesProvider($privateBankClient, $rateTranslator);

// ИЛИ какую-то другую реализацию

// Декорируем реального провайдера, для сохранения полученных курсов в постоянное хранилище
$cbrRatesPersistingProvider = new ExchangeRatesProviderPersistingDecorator($cbrRatesProvider, $repository);

// Декорируем PersistingDecorator, для сохранения курсов в кэш
$cbrRatesCachingProvider = new ExchangeRatesProviderCachingDecorator($cbrRatesPersistingProvider, $cache, 'v1', 10);

// Пользуемся
$rates = $cbrRatesCachingProvider->getRates();
```

Механизм не привязан к конкретной последовательности действий. 
Если вдруг клиентскому коду понадобится выполнять еще какие-то шаги в процессе получения курсов валют, к примеру: 
- накручивать какой-то процент поверх банковского курса (для запаса)
- отправлять курсы на почту бух. отдела (ну, мало-ли :))
- еще какие-то интересные причуды

Все это осуществимо простой реализацией нового декоратора.

Логика сборки конечного экземпляра для использования достаточно объёмная.
Не стал выносить ее в фабрику, т.к. вполне возможно, что разным вызывающим участкам понадобится собрать разные вариации, немного отличающиеся друг от друга.   

Но в принципе, для упрощения старта, можно реализовать фабрику по умолчанию. Если кого-то результат её работы не устравивает, он реализует собственную.

Классы: `\Chetkov\CurrencyRates\Infrastructure\ExchangeRates\Cbr\ApiClient` и `\Chetkov\CurrencyRates\Infrastructure\ExchangeRates\PrivateBank\ApiClient` можно было-бы унифицировать, т.к. они очень похожи и отличаются только: API ссылкой, форматом даты, и логикой трансляции ответа в инфраструктурный ExchangeRateDTO.

Т.е. можно было вынести общую логику, и подменять только: транслятор запроса и транслятор ответа.
Тогда на слое приложения, тоже отпала бы необходимость иметь оба класса: `\Chetkov\CurrencyRates\Application\ExchangeRates\CbrExchangeRatesProvider` и `\Chetkov\CurrencyRates\Application\ExchangeRates\PrivateBankExchangeRatesProvider`.
 Но я не стал этого длать т.к. сейчас логика достаточно примитивна, и возможно, в процессе проработки деталей эти 2 поставщика окажутся достаточно разными, тогда вышеописанная унификация только создаст трудности. 
