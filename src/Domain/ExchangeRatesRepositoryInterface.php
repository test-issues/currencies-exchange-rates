<?php

namespace Chetkov\CurrencyRates\Domain;


/**
 * Class ExchangeRatesRepository
 * @package Chetkov\CurrencyRates
 */
interface ExchangeRatesRepositoryInterface
{
    /**
     * @param \DateTimeImmutable $date
     * @return ExchangeRate[]
     */
    public function findBy(\DateTimeImmutable $date): array;

    /**
     * @param ExchangeRate $exchangeRate
     */
    public function save(ExchangeRate $exchangeRate): void;

    /**
     * @param iterable $currencyRates
     */
    public function saveCollection(iterable $currencyRates): void;
}