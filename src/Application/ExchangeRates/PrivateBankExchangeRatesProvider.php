<?php

namespace Chetkov\CurrencyRates\Application\ExchangeRates;

use Chetkov\CurrencyRates\Application\ExchangeRates\Translator\ExchangeRateDTOTranslator;
use Chetkov\CurrencyRates\Infrastructure\ExchangeRates\PrivateBank\ApiClient;

/**
 * Class PrivateBankExchangeRatesProvider
 * @package Chetkov\CurrencyRates\Application\ExchangeRates
 */
class PrivateBankExchangeRatesProvider implements ExchangeRatesProviderInterface
{
    /** @var ApiClient */
    private $apiClient;

    /** @var ExchangeRateDTOTranslator */
    private $rateTranslator;

    /**
     * CbrExchangeRatesProvider constructor.
     * @param ApiClient $apiClient
     * @param ExchangeRateDTOTranslator $rateTranslator
     */
    public function __construct(ApiClient $apiClient, ExchangeRateDTOTranslator $rateTranslator)
    {
        $this->apiClient = $apiClient;
        $this->rateTranslator = $rateTranslator;
    }

    /**
     * @param \DateTimeImmutable|null $date
     * @return array
     * @throws \Exception
     */
    public function getRates(?\DateTimeImmutable $date = null): array
    {
        $rates = [];
        foreach ($this->apiClient->getRates($date) as $rateDto) {
            $rates[] = $this->rateTranslator->translate($rateDto);
        }
        return $rates;
    }
}
