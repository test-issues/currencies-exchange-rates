<?php

namespace Chetkov\CurrencyRates\Application\ExchangeRates;

use Chetkov\CurrencyRates\Domain\ExchangeRatesRepositoryInterface;

/**
 * Class ExchangeRatesProviderPersistingDecorator
 * @package Chetkov\CurrencyRates\Application\ExchangeRates
 */
class ExchangeRatesProviderPersistingDecorator implements ExchangeRatesProviderInterface
{
    /** @var ExchangeRatesProviderInterface */
    private $decorated;

    /** @var ExchangeRatesRepositoryInterface */
    private $repository;

    /**
     * ExchangeRatesProviderPersistingDecorator constructor.
     * @param ExchangeRatesProviderInterface $decorated
     * @param ExchangeRatesRepositoryInterface $repository
     */
    public function __construct(
        ExchangeRatesProviderInterface $decorated,
        ExchangeRatesRepositoryInterface $repository
    ) {
        $this->decorated = $decorated;
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function getRates(?\DateTimeImmutable $date = null): array
    {
        $date = $date ?? new \DateTimeImmutable();
        $rates = $this->repository->findBy($date);
        if (empty($rates)) {
            $rates = $this->decorated->getRates($date);
            $this->repository->saveCollection($rates);
        }
        return $rates;
    }
}
