<?php

namespace Chetkov\CurrencyRates\Application\ExchangeRates;

use Chetkov\CurrencyRates\Domain\ExchangeRate;
use Psr\SimpleCache\CacheInterface;

/**
 * Class ExchangeRatesProviderCachingDecorator
 * @package Chetkov\CurrencyRates\Application\ExchangeRates
 */
class ExchangeRatesProviderCachingDecorator implements ExchangeRatesProviderInterface
{
    private const DATE_FORMAT = 'Y-m-d';
    private const DEFAULT_CACHE_KEY_PREFIX = 'exchange_rates';
    private const DEFAULT_TTL = 60*60;

    /** @var ExchangeRatesProviderInterface */
    private $decorated;

    /** @var CacheInterface */
    private $cache;

    /** @var string */
    private $cacheKeyPrefix;

    /** @var int */
    private $ttl;

    /**
     * ExchangeRatesProviderCachingDecorator constructor.
     * @param ExchangeRatesProviderInterface $decorated
     * @param CacheInterface $cache
     * @param string $cacheKeyPrefix
     * @param int $ttl
     */
    public function __construct(
        ExchangeRatesProviderInterface $decorated,
        CacheInterface $cache,
        string $cacheKeyPrefix = self::DEFAULT_CACHE_KEY_PREFIX,
        int $ttl = self::DEFAULT_TTL
    ) {
        $this->decorated = $decorated;
        $this->cache = $cache;
        $this->cacheKeyPrefix = $cacheKeyPrefix;
        $this->ttl = $ttl;
    }

    /**
     * @inheritDoc
     */
    public function getRates(?\DateTimeImmutable $date = null): array
    {
        $cacheKey = $this->makeCacheKey($date);
        $serializedRates = $this->cache->get($cacheKey);
        if ($serializedRates) {
            $rates = \unserialize($serializedRates, [ExchangeRate::class]);
        } else {
            $rates = $this->decorated->getRates($date);
            $this->cache->set($cacheKey, \serialize($rates), $this->ttl);
        }
        return $rates;
    }

    /**
     * @param \DateTimeImmutable|null $date
     * @return string
     */
    private function makeCacheKey(?\DateTimeImmutable $date = null): string
    {
        $date = $date ?? new \DateTimeImmutable();
        $formattedDate = $date->format(self::DATE_FORMAT);
        return "{$this->cacheKeyPrefix}.{$formattedDate}";
    }
}
