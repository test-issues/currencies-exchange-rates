<?php

namespace Chetkov\CurrencyRates\Application\ExchangeRates;

use Chetkov\CurrencyRates\Domain\ExchangeRate;

/**
 * Interface ExchangeRatesProviderInterface
 * @package Chetkov\CurrencyRates\Application\ExchangeRates
 */
interface ExchangeRatesProviderInterface
{
    /**
     * @param \DateTimeImmutable|null $date
     * @return ExchangeRate[]
     */
    public function getRates(?\DateTimeImmutable $date = null): array;
}
