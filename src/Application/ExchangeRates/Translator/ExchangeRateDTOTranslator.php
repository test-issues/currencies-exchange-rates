<?php

namespace Chetkov\CurrencyRates\Application\ExchangeRates\Translator;

use Chetkov\CurrencyRates\Domain\ExchangeRate;
use Chetkov\CurrencyRates\Infrastructure\ExchangeRates\ExchangeRateDTO;

/**
 * Class ExchangeRateDTOTranslator
 * @package Chetkov\CurrencyRates\Application\ExchangeRates
 */
class ExchangeRateDTOTranslator
{
    /**
     * Транслирует содержимое из инфраструктурного объекта данных в доменную модель
     *
     * @param ExchangeRateDTO $rateDTO
     * @return ExchangeRate
     */
    public function translate(ExchangeRateDTO $rateDTO): ExchangeRate
    {
        return new ExchangeRate(
            $rateDTO->getSoldCurrencyCode(),
            $rateDTO->getPurchasedCurrencyCode(),
            $rateDTO->getRate(),
            $rateDTO->getDate()
        );
    }
}
