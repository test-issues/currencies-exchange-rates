<?php

namespace Chetkov\CurrencyRates\Infrastructure\Db;

use Chetkov\CurrencyRates\Domain\ExchangeRate;
use Chetkov\CurrencyRates\Domain\ExchangeRatesRepositoryInterface;

/**
 * Имитирует работу с базой храня данные в памяти.
 * Для подключения реальной БД нужно написать другую реализацию интерфейса, к примеру для PG, Mongo и т.д.
 * Место хранения данных можно менять без неоходимости переработки кода.
 *
 * Class ExchangeRatesArrayRepository
 * @package Chetkov\CurrencyRates\Infrastructure\Db
 */
class ExchangeRatesArrayRepository implements ExchangeRatesRepositoryInterface
{
    private const DATE_FORMAT = 'Y-m-d';

    /** @var array */
    private $storage = [];

    /**
     * @inheritDoc
     */
    public function findBy(\DateTimeImmutable $date): array
    {
        $formattedDate = $date->format(self::DATE_FORMAT);
        return $this->storage[$formattedDate] ?? [];
    }

    /**
     * @param ExchangeRate $exchangeRate
     */
    public function save(ExchangeRate $exchangeRate): void
    {
        $formattedDate = $exchangeRate->getDate()->format(self::DATE_FORMAT);
        if (!isset($this->storage[$formattedDate])) {
            $this->storage[$formattedDate] = [];
        }
        $this->storage[$formattedDate][$exchangeRate->getCurrencyPairCode()] = $exchangeRate;
    }

    /**
     * @param ExchangeRate[] $currencyRates
     */
    public function saveCollection(iterable $currencyRates): void
    {
        foreach ($currencyRates as $currencyRate) {
            $this->save($currencyRate);
        }
    }
}
