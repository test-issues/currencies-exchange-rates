<?php

namespace Chetkov\CurrencyRates\Infrastructure\Cache;

use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Реализация локального кэша на основе массива.
 * На самом деле помимо этой реализации ожидается наличие какого то RedisCache или Memcache.
 * НО, текущая вполне имеет право на существование и в некоторых ситуациях оказалась бы очень полезна, к примеру:
 * - есть нагрузка, не маленькая, допустим 6к rpm
 * - запросы приходят на постоянно висящего воркера (демона)
 * - на каждый запрос мы должны получить курсы валют и что-то еще сделать
 * Если у нас первым в цепи будет RedisCache, мы будем его долбить примерно 100 раз в секунду.
 * Если же цепочка будет ArrayCachingDecorator -> RedisCachingDecorator -> DbPersistingDecorator -> RealRatesProvider,
 * то мы избавляемся от избыточной нагрузки на redis.
 * А вообще всё зависит от нюансов.
 *
 * Class ArrayCache
 * @package Chetkov\CurrencyRates\Infrastructure\Cache
 */
class ArrayCache implements CacheInterface
{
    /** @var array */
    private $storage = [];

    /** @var array */
    private $ttl = [];

    /**
     * @inheritDoc
     */
    public function get($key, $default = null)
    {
        $this->deleteIfExpired($key);
        return $this->storage[$key] ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function set($key, $value, $ttl = null): bool
    {
        $this->storage[$key] = $value;
        if (null !== $ttl) {
            $this->ttl[$key] = time() + $ttl;
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function delete($key): bool
    {
        unset($this->storage[$key], $this->ttl[$key]);
        return true;
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        $this->storage = [];
        $this->ttl = [];
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMultiple($keys, $default = null)
    {
        foreach ($keys as $key) {
            yield [$key => $this->get($key, $default)];
        }
    }

    /**
     * @inheritDoc
     */
    public function setMultiple($values, $ttl = null): bool
    {
        $savedKeys = [];
        $allValuesSaved = true;
        foreach ($values as $key => $value) {
            $result = $this->set($key, $value);
            if ($result) {
                $savedKeys[] = $key;
            } else {
                $allValuesSaved = false;
                $this->deleteMultiple($savedKeys);
                break;
            }
        }
        return $allValuesSaved;
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple($keys): bool
    {
        foreach ($keys as $key) {
            $result = $this->delete($key);
            if (!$result) {
                return false;
            }
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function has($key): bool
    {
        $this->deleteIfExpired($key);
        return isset($this->storage[$key]);
    }

    /**
     * @param $key
     * @throws InvalidArgumentException
     */
    private function deleteIfExpired($key): void
    {
        if (isset($this->ttl[$key]) && $this->ttl[$key] < time()) {
            $this->delete($key);
        }
    }
}
