<?php

namespace Chetkov\CurrencyRates\Infrastructure\ExchangeRates;

/**
 * Class ExchangeRateDTO
 * @package Chetkov\CurrencyRates\Infrastructure\ExchangeRates
 */
class ExchangeRateDTO
{
    /** @var string */
    private $soldCurrencyCode;

    /** @var string */
    private $purchasedCurrencyCode;

    /** @var float */
    private $rate;

    /** @var \DateTimeImmutable */
    private $date;

    /**
     * ExchangeRate constructor.
     * @param string $soldCurrencyCode
     * @param string $purchasedCurrencyCode
     * @param float $rate
     * @param \DateTimeImmutable|null $date
     */
    public function __construct(
        string $soldCurrencyCode,
        string $purchasedCurrencyCode,
        float $rate,
        ?\DateTimeImmutable $date = null
    ) {
        $this->soldCurrencyCode = $soldCurrencyCode;
        $this->purchasedCurrencyCode = $purchasedCurrencyCode;
        $this->rate = $rate;
        $this->date = $date ?? new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getSoldCurrencyCode(): string
    {
        return $this->soldCurrencyCode;
    }

    /**
     * @return string
     */
    public function getPurchasedCurrencyCode(): string
    {
        return $this->purchasedCurrencyCode;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }
}
