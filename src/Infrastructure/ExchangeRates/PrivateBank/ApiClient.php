<?php

namespace Chetkov\CurrencyRates\Infrastructure\ExchangeRates\PrivateBank;

use Chetkov\CurrencyRates\Infrastructure\ExchangeRates\ExchangeRateDTO;
use Psr\Log\LoggerInterface;

/**
 * Class ApiClient
 * @package Chetkov\CurrencyRates\Infrastructure\ExchangeRates\PrivateBank
 */
class ApiClient
{
    private const API_URL = 'https://api.privatbank.ua/p24api/exchange_rates?json&date=';
    private const DATE_FORMAT = 'd.m.Y';

    /** @var LoggerInterface */
    private $logger;

    /**
     * ApiClient constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param \DateTimeImmutable|null $date
     * @return ExchangeRateDTO[]
     * @throws \Exception
     */
    public function getRates(?\DateTimeImmutable $date = null): iterable
    {
        $response = json_decode($this->executeRequest($date), false);
        $currentCurrency = $response->baseCurrencyLit;
        foreach ($response->exchangeRate as $exchangeRateData) {
            if (!$exchangeRateData->currency) {
                continue;
            }
            yield new ExchangeRateDTO(
                $exchangeRateData->currency,
                $currentCurrency,
                $exchangeRateData->saleRateNB,
                $date
            );
        }
    }

    /**
     * @param \DateTimeImmutable|null $date
     * @return string
     * @throws \Exception
     */
    protected function executeRequest(?\DateTimeImmutable $date = null): string
    {
        $date = $date ?? new \DateTimeImmutable();
        $apiUrl = self::API_URL . $date->format(self::DATE_FORMAT);
        $this->logger->notice(sprintf('Request: GET %s', $apiUrl));

        //Не стал прикручивать какой-то http клиент, т.к. для этого обменника будет слишком жирно :)
        $content = file_get_contents($apiUrl);
        $this->logger->notice(sprintf('Response: %s', $apiUrl), [
            'content' => $content,
        ]);

        if ($content === false) {
            //Все зависит от логики приложения. Если процесс получения курса валют НЕ ДОЛЖЕН ломать процесс, то
            //исключение нужно отловить уровнем выше, и предпринять необходимые действия.
            //В таком случае лучше будет босать более специализированное исключение (типа: CbrProviderException)
            throw new \RuntimeException('Can not get exchange rates. Provider: ' . get_class($this));
        }

        return $content;
    }
}
