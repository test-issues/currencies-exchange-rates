<?php

namespace Chetkov\CurrencyRates\Infrastructure\ExchangeRates\Cbr;

use Chetkov\CurrencyRates\Infrastructure\ExchangeRates\ExchangeRateDTO;
use Psr\Log\LoggerInterface;

/**
 * Class ApiClient
 * @package Chetkov\CurrencyRates\Infrastructure\ExchangeRates\Cbr
 */
class ApiClient
{
    private const API_URL = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=';
    private const DATE_FORMAT = 'd/m/Y';
    private const CURRENT_CURRENCY = 'RUB';

    /** @var LoggerInterface */
    private $logger;

    /**
     * ApiClient constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param \DateTimeImmutable|null $date
     * @return ExchangeRateDTO[]
     * @throws \Exception
     */
    public function getRates(?\DateTimeImmutable $date = null): iterable
    {
        $response = $this->executeRequest($date);

        $dom = new \DOMDocument();
        $dom->loadXML($response);
        /** @var \DOMElement $valuteElement */
        foreach ($dom->getElementsByTagName('Valute') as $valuteElement) {
            $currencyCode = $valuteElement->getElementsByTagName('CharCode')[0]->nodeValue;
            $rate = $valuteElement->getElementsByTagName('Value')[0]->nodeValue;
            $rate = (float)str_replace(',', '.', $rate);

            yield new ExchangeRateDTO($currencyCode, self::CURRENT_CURRENCY, $rate, $date);
        }
    }

    /**
     * @param \DateTimeImmutable|null $date
     * @return string
     * @throws \Exception
     */
    protected function executeRequest(?\DateTimeImmutable $date = null): string
    {
        $date = $date ?? new \DateTimeImmutable();
        $apiUrl = self::API_URL . $date->format(self::DATE_FORMAT);
        $this->logger->notice(sprintf('Request: GET %s', $apiUrl));

        //Не стал прикручивать какой-то http клиент, т.к. для этого обменника будет слишком жирно :)
        $content = file_get_contents($apiUrl);
        $this->logger->notice(sprintf('Response: %s', $apiUrl), [
            'content' => $content,
        ]);

        if ($content === false) {
            //Все зависит от логики приложения. Если процесс получения курса валют НЕ ДОЛЖЕН ломать процесс, то
            //исключение нужно отловить уровнем выше, и предпринять необходимые действия.
            //В таком случае лучше будет босать более специализированное исключение (типа: CbrProviderException)
            throw new \RuntimeException('Can not get exchange rates. Provider: ' . get_class($this));
        }

        return $content;
    }
}
